---
author: Mateusz Janiszewski
title: Proces tworzenia gier komputerowych
subtitle:
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Historia

Za pierwszą grę komputerową jest uznawane   ***OXO*** (kółko i krzyżyk), które pierwszy raz zostało uruchomione w 1950 roku na komputerze  \textcolor{red}{Bertie the Brain}.

Za pierwszą **grę wideo** uważa się ***Spacewar!***. Została ona utworzona w 1961 roku przez grupę studentów z \textcolor{blue}{Massachusetts Institute of Technology}. Gra pozwalała na dwuosobową bitwę statków kosmicznych.

## Rodzaje gier komputerowych

1. Gry zręcznościowe
  + Strzelanki
  	+ Strzelanki pierwszoosobowe i trzecioosobowe
  + Gry platformowe
  + Przygodowe gry akcji
2. Gry przygodowe
3. Gry fabularne
	+ Hack and slash
	+ MMORPG
4. Gry symulacyjne
5. Gry sportowe
	+ Gry wyścigowe
6. Gry strategiczne
	+ Gry strategiczne turowe
	+ Strategiczne gry czasu rzeczywistego
7. Gry przeglądarkowe
8. Gry logiczne
9. Gry edukacyjne

## Gry indie

### Mała gra komputerowa
Jest to rodzaj gry komputerowej, która została stworzona przez pojedynczą osobę lub mały zespół.

###
Są one zazwyczaj mało rozbudowane i krótkie.

###
Zwykle nie mają dofinansowania żadnego wydawcy.

## Podział ról w zespole tworzącym grę

\begin{alertblock}{Warstwa wydawnicza i kapitałowa}
Wydawca

Sponsor
\end{alertblock}

\begin{block}{Warstwa wykonawcza}
Projektant gry

Programista

Grafik

Realizator dźwięku

Tester
\end{block}

\begin{exampleblock}{Warstwa dystrybucyjna}
Specjalista ds. Marketingu

Specjalista ds. Reklamy 
\end{exampleblock}

## Najpopularniejsze języki programowania gier komputerowych


\begin{center}C++ \end{center}
\lstinputlisting[language=c++, linerange={1-6}]{src/hello.cpp}



\begin{center}Java \end{center}
```java
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
}
```

## Podział procesów produkcji

::: {.columns}
:::: {.column width=0.3}
\begin{center}Preprodukcja \end{center}

+ Zebranie odpowiedniego zespołu
+ Ułożenie koncepcji gry
+ Wybór odpowiedniego silnika
+ Stworzenie prototypu gry
::::
:::: {.column width=0.3}
\begin{center}Produkcja właściwa \end{center}
  
+ Tworzenie silnika gry
+ Tworzenie poziomów
+ Tworzenie audio
+ Testowanie
::::
:::: {.column width=0.35}
\begin{center}Postprodukcja \end{center}
  
+ Tworzenie łat na kod źródłowy gry
+ Poprawa wydajności
+ Dostosowywanie gry do nowszych systemów operacyjnych
+ Przerabianie gry pod inne platformy
::::
:::

## Jak wygląda proces tworzenia gier komputerowych?


![](pics/zdj1.jpg){ height=45% width=45%}

![](pics/zdj2.jpg){ height=40% width=40%}

![](pics/zdj3.jpg){ height=50% width=50%}

## Statystyki wybranych gier

| Gra        	   | Wydawca         | Czas produkcji |Budżet   |
| :---------------:|:---------------:| :-------------:|:-------:|
| Wiedźmin 3       | CD Project Red  |4 lata		  |$81 mln. |
| GTA V     	   | Rockstar Games  |3 lata 		  |$265 mln.|
| Minecraft 	   | Mojang Studios  |1.5 roku 		  |<$1 mln. |
| Super Mario Bros.| Nintendo	     |2 lata 		  |$48 mln.	|
